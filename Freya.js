const fs = require('fs');
const webui = require('./webui');
const CircadianSchedule = require('./CircadianSchedule');
const TemperatureController = require('./TemperatureController');
const LightingController = require('./LightingController');
const HumidityController = require('./HumidityController');
const Qcom = require('./Qcom');

const freyasys = require('./freyasys');
const freyaio = require('./freyaio');
const freyahmi = require('./freyahmi');
freyaio.init();
freyahmi.init();

var configfile = './config/climatecore.conf';	// default config file

// read config file path from argument, otherwise use default config file
if( process.argv[2] != null ) configfile = process.argv[2];
try{
	var settings = JSON.parse(cleanString(fs.readFileSync(configfile, 'utf8')));
} catch(err){
	setTimeout(function(){
	freyahmi.setLedOne("red");		// Critical system state!
	freyahmi.setBuzzer("on");
	console.error("corrupt config file; exit");
	setTimeout(function(){
		process.exit(1);
		}, 100);
	}, 250);
}

// remove unwanted characters from a string
// (used for e.g. cleaning up the UTF-8 BOM from a file)
function cleanString(input) {
	var output = "";
	for (var i=0; i<input.length; i++) {
		if (input.charCodeAt(i) <= 127) {
			output += input.charAt(i);
		}
	}
	return output;
}

// Read Device UUID
var deviceID = freyasys.getUuid();
console.log("Device ID: "+deviceID);

var circadianSchedule = new CircadianSchedule( settings );

var temperatureController = new TemperatureController();
var lightingController = new LightingController();
var humidityController = new HumidityController();

var sensor = new Qcom("freyaSensor_1");
var sensorLegacy = new Qcom("ES_sensor_1");	// sensor legacy support

var noSensorMode = false;
var noLegacySensorMode = false;

/* initialize a heartbeat */
setTimeout(function(){
	freyahmi.setLedOne("orange", 500);	// this might cause timing issues with the "connected" event from the sensor
	freyasys.checkInternet();
}, 200);

/* Circadian Rythme */
circadianSchedule.on('newTimeOfDay', function( timeOfDay ){

	console.log("----------- new settings -----------");
	console.log("timeOfDay: "+ timeOfDay.name);
	console.log("from "+timeOfDay.startHours+":"+timeOfDay.startMinutes);
	console.log("until "+timeOfDay.endHours+":"+timeOfDay.endMinutes);
	console.log("humidity:");
	console.log("\trainInterval: "+timeOfDay.humidity.rainInterval+" [mins]");
	console.log("\trainDuration: "+timeOfDay.humidity.rainDuration+" [sec]");
	console.log("\tminRhumidity: "+timeOfDay.humidity.minRhumidity+" [%]");
	console.log("\tmaxRhumidity: "+timeOfDay.humidity.maxRhumidity+" [%]");
	console.log("lighting:");
	console.log("\tminIntensity: "+timeOfDay.lighting.minIntensity+" [%]");
	console.log("\tmaxIntensity: "+timeOfDay.lighting.maxIntensity+" [%]");
	console.log("temperature:");
	console.log("\tminTemperature: "+timeOfDay.temperature.minTemperature+" [deg.C]");
	console.log("\tmaxTemperature: "+timeOfDay.temperature.maxTemperature+" [deg.C]");
	console.log("-----------------------------------");

	// set webui "season" to "timeOfDay" until we've implemented season =D
	webui.setCurrentSeason(timeOfDay.name);

	lightingController.settings( timeOfDay.lighting.minIntensity, timeOfDay.lighting.maxIntensity);
	temperatureController.settings( timeOfDay.temperature.minTemperature, timeOfDay.temperature.maxTemperature );
	humidityController.settings( timeOfDay.humidity.rainInterval, timeOfDay.humidity.rainDuration, timeOfDay.humidity.minRhumidity, timeOfDay.humidity.maxRhumidity );
});

/* Temperature Controller Output */
temperatureController.on("heater", function( data ){
	//console.log("to heater: "+data);
	freyaio.setHeater( data );
});

temperatureController.on("cooler", function( data ){
	// TODO to actuator
	//console.log("to cooler: "+data);
});

/* Lighting Controller Output */
lightingController.on("lights", function( data ){
	//console.log("to lights: "+data);
	freyaio.setLighting( data );
});

/* Humidity Controller Output */
humidityController.on("sprinklers", function( data ){
	//console.log("to sprinklers: "+data);
	freyaio.setSprinklers( data );
});

/* Freya HMI button */
freyahmi.button.on("action", function( seconds ){
	if( seconds <=2 ){
		// TODO reload settings?
	}
	else if( seconds <= 4 ){
      		// slow blink the system LED red
  		freyahmi.setLedOne("red", 400);
		// reboot system
		freyasys.reboot();
	}
	else if( seconds >= 7 ){
  		// fast blink the system LED red
  		freyahmi.setLedOne("red", 100);
		// halt system
		freyasys.shutdown();
	}
});

/* Check for internet */
setInterval(function(){
	freyahmi.setLedTwo("orange");
	freyasys.checkInternet();
}, 30000);

// TODO cleanup freyasys event emitter!
freyasys.events.on("internet", function( connection ){
	if( connection == "connected" ){
		freyahmi.setLedTwo("green");
	}
});

/* Sensor */
sensor.on('disconnected', function(){
	if( noLegacySensorMode && !noSensorMode ){
		freyahmi.setLedOne("orange", 500);

		temperatureController.noSensor(true);
		humidityController.noSensor(true);
		lightingController.noSensor(true);

		console.log("sensor disconnected; switched to noSensorMode");
	}
	if(!noSensorMode){
		noSensorMode = true;
	}
});

sensor.on('connected', function(){
	freyahmi.setLedOne("green", 500);
	noSensorMode = false;

	temperatureController.noSensor(false);
	humidityController.noSensor(false);
	lightingController.noSensor(false);

	console.log("sensor connected");
});

sensor.on('data', function( data ){
	if( data.signal == "humidity" ){
		humidityController.setCurrent( data.argument );
		webui.setCurrentHumidity( data.argument );
	}
	else if( data.signal == "lighting" ){
		lightingController.setCurrent( data.argument );
		webui.setCurrentLighting( data.argument );
	}
	else if (data.signal == "temperature" ){
		temperatureController.setCurrent( data.argument );
		webui.setCurrentTemperature( data.argument );
	}
});

/* sensor Legacy */
sensorLegacy.on('disconnected', function(){	
	if( !noLegacySensorMode && noSensorMode ){
		freyahmi.setLedOne("orange", 500);

		temperatureController.noSensor(true);
		humidityController.noSensor(true);
		lightingController.noSensor(true);

		console.log("sensorLegacy disconnected; switched to noSensorMode");
		webui.setSensorStatus("error");
	}

	if(!noLegacySensorMode){
		noLegacySensorMode = true;
	}
});

sensorLegacy.on('connected', function(){
	freyahmi.setLedOne("green", 500);
	noLegacySensorMode = false;

	temperatureController.noSensor(false);
	humidityController.noSensor(false);
	lightingController.noSensor(false);

	webui.setSensorStatus("ok");
	console.log("sensorLegacy connected");
});

sensorLegacy.on('data', function( data ){
	if( data.signal == "humidity" ){
		humidityController.setCurrent( data.argument );
		webui.setCurrentHumidity( data.argument );
	}
	else if( data.signal == "lighting" ){
		lightingController.setCurrent( data.argument );
		webui.setCurrentLighting( data.argument );
	}
	else if (data.signal == "temperature" ){
		temperatureController.setCurrent( data.argument );
		webui.setCurrentTemperature( data.argument );
	}
});




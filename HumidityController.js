/*
 *
 *	written by:	Tom 'SpuQ' Santens
 *		on:	17/04/2019 - based on 31/05/2017
 */

var EventEmitter = require('events').EventEmitter;
var util = require('util');

util.inherits(HumidityController, EventEmitter);

module.exports = HumidityController;

function HumidityController(){
	var interval;
	var duration;
	var maximum;
	var minimum;
	var current;
	var intervalTimer;
	var noSensorMode = true;


	this.settings = function( interval, duration, minRhumidity, maxRhumidity ){
		this.interval = interval;
		this.duration = duration;
		this.minimum = minRhumidity;
		this.maximum = maxRhumidity;

		clearInterval(this.intervalTimer);	// kill the previous interval
		this.emit("sprinklers", "off"); 	// make sure the sprinklers are off

		if( this.interval <= 0 ){
			// no sprinkler action!
		}
		else {
			this.intervalTimer = setInterval( timedSprinklers, this.interval*60000, this);
		}
	}

	// handle data from sensor
	this.setCurrent = function( value ){
		this.current = value;
	}

	this.noSensor = function( value ){
		this.noSensorMode = value;
	}

	// function called by the timer
	function timedSprinklers( hc ){
		if( !hc.noSensorMode ){
			//TODO make use of the sensor data
			hc.emit("sprinklers", "on");

			setTimeout( function( hc ){
				hc.emit("sprinklers", "off");
			}, hc.duration*1000, hc );
		}
		else{
			hc.emit("sprinklers", "on");

			setTimeout( function( hc ){
				hc.emit("sprinklers", "off");
			}, hc.duration*1000, hc );
		}
	}
}


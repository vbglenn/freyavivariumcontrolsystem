/*

 *
 *	written by:	Tom 'SpuQ' Santens
 *		on:	16/04/2019 - based on 31/05/2017
 */
var EventEmitter = require('events').EventEmitter;
var util = require('util');

util.inherits(LightingController, EventEmitter);

module.exports = LightingController;

function LightingController(){
	var minIntensity;
	var maxIntensity;

	var current;
	var noSensorMode = true;

	setInterval( regulator, 2000, this);

	var state;

	this.settings = function(minIntensity, maxIntensity){
		this.minIntensity = minIntensity;
		this.maxIntensity = maxIntensity;
	}

	this.setCurrent = function( value ){
		this.current = value;
	}

	this.noSensor = function( value ){
		this.noSensorMode = value;
	}

	// core function
	function regulator( lc ){
		if( !lc.noSensorMode ){
			// TODO a mode that actually uses sensor data
			if( lc.maxIntensity > 50){
				lc.emit("lights", "on");
			}
			else{
				lc.emit("lights", "off");
			}
		}
		else{
			if( lc.maxIntensity > 50){
				lc.emit("lights", "on");
			}
			else{
				lc.emit("lights", "off");
			}
		}
	}
}


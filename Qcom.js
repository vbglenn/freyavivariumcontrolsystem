var net = require('net');
var readline = require('readline');
var EventEmitter = require('events').EventEmitter;
var util = require('util');

util.inherits(Qcom, EventEmitter);

module.exports = Qcom;

function Qcom( deviceName ){
	var socketPath = "/Qdev/"+deviceName;
	var socket;
	var connected = false;

	// whenever the socket is closed, we'll try to reconnect every second	
	setInterval(function( qc ){
		if(connected == false){
			connect( qc );
		}
	},1000, this);


	function connect( qc ){
		socket = net.createConnection( socketPath, function(){
			connected = true;
			qc.emit("connected");
		});

		socketLineIn = readline.createInterface( { terminal : false, input: socket } );

		socketLineIn.on('line', function(line){
			var obj = JSON.parse(line);
			qc.emit("data", obj );
		});


		socket.on('connect', function(){ });

		socket.on('error', function( err ){ });

		socket.on('data', function( data ){ });

		socket.on('end', function(){ });

		// event is called anytime the socket is closed (e.g. error, ...)
		socket.on('close', function(){
			connected = false;
			qc.emit("disconnected");
		});

	}

	// TODO send to device!!! (is not relevant for the sensor right now...)
	module.exports.send = function( msg, topic ){
		console.log("sending: "+msg+":"+topic);
		// encapsulate
	}
}


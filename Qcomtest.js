var Qcom=require('./Qcom.js');

sensor = new Qcom("freyaSensor_1");
console.log('running...');

sensor.on("data", function(data){
	console.log("got data from device: "+JSON.stringify(data));
});

sensor.on('disconnected', function(){
	console.log("we're disconnected boys");
});

sensor.on('connected', function(){
	console.log("we're connected! Let's go!");
});

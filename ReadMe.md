# Freya - Vivarium Control System
Every lifeform has its own environmental needs. Freya is an Open Source, Raspberry Pi based control system for *coding climate* in your vivarium/terrarium/cityfarm/... .
![Freya setup](images/FreyaSetup.jpg)
## Recommended hardware
*   Raspberry Pi 3 or higher, and a SD card with Raspbian installed
*   [Freya Controller PCB](https://www.tindie.com/products/spuq/freya-controller-pcb/)
*   [Freya Sensor Module](https://www.tindie.com/products/spuq/freya-sensor-module/)

![Freya BaseKit](images/FreyaBaseKit.jpg)
## Install / Uninstall
You'll need a Raspberry Pi and a SD card with Raspbian installed. I also recommend to configure a static IP, and to make sure the timezone is correct. Installing Freya requires an internet connection.

### Install
SSH into your Raspberry Pi, and download and install this software
```bash
wget https://gitlab.com/SpuQ/freyavivariumcontrolsystem/-/archive/master/freyavivariumcontrolsystem-master.tar
tar -xvf freyavivariumcontrolsystem-master.tar
cd freyavivariumcontrolsystem-master
sudo sh install.sh
```
When all is done, reboot your system.
```bash
sudo shutdown -r now
```

### Uninstall
Navigate to the installation directory of the Freya system, and run the uninstall script.
```bash
cd /opt/Freya/
sudo sh uninstall.sh
```
## Contribute
There's lots of room for improvement, and plenty of possibilities to grow the system. Feel like playing along? Go for it, and let me know!
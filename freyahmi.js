/*
 *	Freya HMI
 */

var gpio = require('rpi-gpio');
var events = require('events');
var eventEmitter = new events.EventEmitter();

var statusOneGreenOut = 37;	// GPIO26
var statusOneRedOut = 35;	// GPIO19
var statusTwoGreenOut = 32;	// GPIO12 - Freya REV1.1
var statusTwoRedOut = 33;	// GPIO13
var buttonIn = 31;		// GPIO6
var buzzerOut = 29;		// GPIO5

exports.init = function(){
	gpio.setup( statusOneGreenOut, gpio.DIR_LOW );	// Initialize LED outputs
	gpio.setup( statusOneRedOut, gpio.DIR_LOW );
	gpio.setup( statusTwoGreenOut, gpio.DIR_LOW );
	gpio.setup( statusTwoRedOut, gpio.DIR_LOW );
	gpio.setup( buzzerOut, gpio.DIR_LOW );		// Initialize buzzer

	// Initialize button input and interrupt
	gpio.on('change', function(channel, value) {
		buttonOnInterrupt();
	});
	gpio.setup( buttonIn, gpio.DIR_IN, gpio.EDGE_FALLING);

	// Initialize Status LEDs to orange
	setTimeout( function(){
		setStatusOneOrange();
		setStatusTwoOrange();
	}, 100);
}

/*
 *	button
 */
exports.button = eventEmitter;

var buttonPressed = 0;

function buttonOnInterrupt(){
	// check whether button is pressed
	gpio.read( buttonIn, function(err, value){
		if( value == false && buttonPressed == 0 ){
			// if the button is pressed, and we're not handling a press yet, start handling
			buttonPressed = 1;
			setTimeout(function(){
				buttonHandler();
			}, 500); // debounce timeout
		}
	});
}

var buttonHoldCounter = 1;
function buttonHandler(){
	gpio.read( buttonIn, function(err, value){
		if( value == true ){
			eventEmitter.emit("action", buttonHoldCounter/2);
			buttonPressed = 0;
			buttonHoldCounter = 1;
		}
		else {
			buttonHoldCounter ++;
			setTimeout( buttonHandler, 500);
		}
	});
}

/*
 *	buzzer
 */
var buzzerInterval;
var buzzerOn = false;

exports.setBuzzer = function( speedMs ){
	killBuzzer();
	if( speedMs == 0 || speedMs == "off" ){
		setBuzzerOff();
	}
	else if( speedMs == "on"){
		setBuzzerOn();
	}
	else{
		buzzerInterval = setInterval( function(){
			if( !buzzerOn ){
				buzzerOn = true;
				setBuzzerOn();
			}
			else {
				buzzerOn = false;
				setBuzzerOff();
			}
		}, speedMs);
	}
}

function setBuzzerOff(){
	gpio.write( buzzerOut, false );
}

function setBuzzerOn(){
	gpio.write( buzzerOut, true );
}

function killBuzzer(){
	clearInterval( buzzerInterval );
	setBuzzerOff();
}

/*
 *	Status LED 1
 */
exports.setLedOne = function( color, blinkSpeed){
	// if LED is blinking, kill it
	killBlinkOne();
	// if no blinkspeed is defined
	if( !blinkSpeed ){
		switch( color ){
			case "green":
			setStatusOneGreen();
			break;
			case "orange":
			setStatusOneOrange();
			break;
			case "red":
			setStatusOneRed();
			break;
		}
	}
	// if a blinkspeed is defined
	else {
		switch( color ){
			case "green":
			setBlinkOne("green", "off", blinkSpeed);
			break;
			case "orange":
			setBlinkOne("orange", "off", blinkSpeed);
			break;
			case "red":
			setBlinkOne("red", "off", blinkSpeed);
			break;
		}
	}
}

function setStatusOneGreen(){
	gpio.write( statusOneGreenOut, true );
	gpio.write( statusOneRedOut, false );
}

function setStatusOneOrange(){
	gpio.write( statusOneGreenOut, true );
	gpio.write( statusOneRedOut, true );
}

function setStatusOneRed(){
	gpio.write( statusOneGreenOut, false );
	gpio.write( statusOneRedOut, true );
}

function setStatusOneOff(){
	gpio.write( statusOneGreenOut, false );
	gpio.write( statusOneRedOut, false );
}

/* blink function */
var blinkOneInterval;
var blinkOneOn = 0;
var blinkOneColorOne;
var blinkOneColorTwo;

function setBlinkOne(colorOne, colorTwo, speedMs){
	blinkOneColorOne = colorOne;
	blinkOneColorTwo = colorTwo;

	 blinkOneInterval = setInterval(function(){
		if( !blinkOneOn ){
			blinkOneOn = 1;
			switch( blinkOneColorOne ){
				case "green":
					setStatusOneGreen();
					break;
				case "orange":
					setStatusOneOrange();
					break;
				case "red":
					setStatusOneRed();
					break;
				default:
					setStatusOneOff();
					break;
			}
		}
		else {
			blinkOneOn = 0;
			switch( blinkOneColorTwo ){
				case "green":
				setStatusOneGreen();
				break;
				case "orange":
				setStatusOneOrange();
				break;
				case "red":
				setStatusOneRed();
				break;
				default:
				setStatusOneOff();
				break;
			}
		}
	}, speedMs);
}

function killBlinkOne(){
	clearInterval( blinkOneInterval );
	setStatusOneOff();
}

/*
 *	Status LED 2
 */
exports.setLedTwo = function( color, blinkSpeed){
	// if LED is blinking, kill it
	killBlinkTwo();
	// if no blinkspeed is defined
	if( !blinkSpeed ){
		switch( color ){
			case "green":
			setStatusTwoGreen();
			break;
			case "orange":
			setStatusTwoOrange();
			break;
		}
	}
	// if a blinkspeed is defined
	else {
		setBlinkTwo( blinkSpeed );
	}
}

function setStatusTwoOrange(){
	gpio.write( statusTwoRedOut, true );
}

function setStatusTwoGreen(){
	gpio.write( statusTwoRedOut, false );
}
/* blink function */
var blinkTwoInterval;
var blinkTwoOn = 0;

function setBlinkTwo( speedMs ){
	blinkTwoInterval = setInterval(function(){
		if( !blinkOneOn ){
			blinkTwoOn = 1;
				setStatusTwoOrange();
		}
		else {
			blinkTwoOn = 0;
			setStatusTwoGreen();
		}
	}, speedMs);
}

function killBlinkTwo(){
	clearInterval( blinkTwoInterval );
	setStatusTwoGreen();
}

var gpio = require('rpi-gpio');

var heaterOut = 11;		// GPIO17
var sprinklersOut = 13;		// GPIO27
var lightingOut = 15;		// GPIO22

exports.init = function(){
	// Initialize GPIO
	gpio.setup( heaterOut, gpio.DIR_OUT);
	gpio.setup( lightingOut, gpio.DIR_OUT);
	gpio.setup( sprinklersOut, gpio.DIR_OUT);

	// Initialize Controller Outputs
	setTimeout(function(){
		setHeaterOff();
		setLightingOff();
		setSprinklersOff();
	}, 200);
}


/*	Controller Outputs	*/

exports.setHeater = function( state ){
	switch( state ){
		case "on":
		setHeaterOn();
		break;
		case "off":
		setHeaterOff();
		break;
		default:
		console.log("invalid value");
		break;
	}
}

exports.setSprinklers = function( state ){
	switch( state ){
		case "on":
		setSprinklersOn();
		break;
		case "off":
		setSprinklersOff();
		break;
		default:
		console.log("invalid value");
		break;
	}
}

exports.setLighting = function( state ){
	switch( state ){
		case "on":
		setLightingOn();
		break;
		case "off":
		setLightingOff();
		break;
		default:
		console.log("invalid value");
		break;
	}
}

function setHeaterOn(){
	gpio.write( heaterOut, true );
}

function setHeaterOff(){
	gpio.write( heaterOut, false );
}

function setSprinklersOn(){
	gpio.write( sprinklersOut, true );
}

function setSprinklersOff(){
	gpio.write( sprinklersOut, false );
}

function setLightingOn(){
	gpio.write( lightingOut, true );
}

function setLightingOff(){
	gpio.write( lightingOut, false );
}

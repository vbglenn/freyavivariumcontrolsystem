const exec = require('child_process').exec;
const fs = require('fs');
var events = require('events');
var eventEmitter = new events.EventEmitter();

exports.events = eventEmitter;

exports.reboot = function(){
	console.log("Rebooting in 3s");
	// emit reboot message for all who needs to know
	eventEmitter.emit("reboot");
	// execute reboot command in 3 seconds
	setTimeout(function(){
		exec("shutdown -r now");
	}, 3000 );
}

exports.shutdown = function(){
	console.log("Shutting down in 3s");
	// emit shutdown message for all who needs to know
	eventEmitter.emit("shutdown");
	// execute shutdown command in 3 seconds
	setTimeout(function(){
		exec("shutdown -h now");
	}, 3000 );
}

exports.checkInternet = function(){
	// TODO not a scalable solution! eventually this will start to look
	// like a DDoS attack!...
	exec('ping -c 1 8.8.8.8', function(error, stdout, stderr){
		if(error !== null){
			eventEmitter.emit("internet","disconnected");
		}
		else{
			eventEmitter.emit("internet","connected");
		}
	});
}

// UUID from EEPROM - Raspberry Pi feature for hats
// in /proc/device-tree/hat/uuid
exports.getUuid = function(){
	var uuid;
	try{
		uuid = fs.readFileSync('/proc/device-tree/hat/uuid');
	} catch( err ){
		return null;
	}
	return uuid;
}

const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const fs = require('fs');
const port = 80;
const cors = require('cors')

const configFile = "config/climatecore.conf";		// path to config file

app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(cors())
app.listen(port, () => console.log(`Example app listening on port ${port}!`));

app.get('/configInterface', function( req, res) {
	res.sendFile('configInterface.html');
});

app.get('/api/settings', function( req, res ){
	console.log("GET settings");
	let settings = JSON.parse(fs.readFileSync(configFile));
	res.send(settings);
});

// write the new configuration to config file
// (and restart the controller?)
app.post('/api/settings', function( req, res ){
	var settings = req.body;
	console.log(settings);
	fs.writeFile(configFile, JSON.stringify(settings));
	res.send("blub");
});

/*	Current temperature/humidity/lighting	*/
var temperatureNow=0;
var humidityNow=0;
var lightingNow=0;
var seasonNow="noSeason";
var sensorStatus="error";
var temperatureGraph=[];
var humidityGraph=[];
var lightingGraph=[];

app.get('/api/temperature/current', function( req, res){
	res.send(JSON.parse('{"temperature":'+temperatureNow+'}'));
});

app.get('/api/humidity/current',function( req, res){
	res.send(JSON.parse('{"humidity":'+humidityNow+'}'));
});


app.get('/api/lighting/current',function( req, res){
	res.send(JSON.parse('{"lighting":'+lightingNow+'}'));
});

module.exports.setCurrentTemperature = function( temperature ){
	temperatureNow = temperature;
}

module.exports.setCurrentHumidity = function( humidity ){
	humidityNow = humidity;
}

module.exports.setCurrentLighting = function( lighting ){
	lightingNow = lighting;
}

module.exports.setCurrentSeason = function( season ){
	seasonNow = season;
}

app.get('/api/season/current',function( req, res){
	var data = {};
	data.season = seasonNow;
	res.send(data);
});

app.get('/api/lighting/status',function( req, res){
	var data = {};
	data.status = sensorStatus;
	res.send(data);
});

app.get('/api/humidity/status',function( req, res){
	var data = {};
	data.status = sensorStatus;
	res.send(data);
});

app.get('/api/temperature/status',function( req, res){
	var data = {};
	data.status = sensorStatus;
	res.send(data);
});

/*	graphs		*/
app.get('/api/lighting/graph',function( req, res){
	var data = lightingGraph;
	res.send(data);
});

app.get('/api/humidity/graph',function( req, res){
	var data = humidityGraph;
	res.send(data);
});

app.get('/api/temperature/graph',function( req, res){
	var data = temperatureGraph;
	res.send(data);
});

// sample data for graph
var graphSampleInterval = 10;	// seconds
var windowSize = 500;		// number of graph points we're saving
setInterval(function(){
	var timestamp = Date.now();

	updateTemperatureGraph( timestamp );
	updateHumidityGraph( timestamp );
	updateLightingGraph( timestamp );
}, graphSampleInterval*1000);

function updateTemperatureGraph( timestamp ){
	var datapoint = {};
	datapoint.temperature = temperatureNow;
	datapoint.timestamp = timestamp;
	temperatureGraph.push(datapoint);
}

function updateHumidityGraph( timestamp ){
	var datapoint = {};
	datapoint.humidity = humidityNow;
	datapoint.timestamp = timestamp;
	humidityGraph.push(datapoint);
}

function updateLightingGraph( timestamp ){
	var datapoint = {};
	datapoint.lighting = lightingNow;
	datapoint.timestamp = timestamp;
	lightingGraph.push(datapoint);
}

module.exports.setSensorStatus = function( status ){
	sensorStatus = status;
}
